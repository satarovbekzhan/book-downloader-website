//make the background of navigation bar black and go-to-top button sown when scrolling down
$(window).scroll(function() {
   if($(window).scrollTop() + $(window).height() > $(document).height() - ($(document).height() - 1100)) {
       $('#navbar').css({'background-color':'rgba(0, 0, 0, .95)','box-shadow':'0 0 50px #696969'});
       document.getElementById("top").style.display = "block";
   }
   else {
   		$('#navbar').css({'background-color':'rgba(0, 0, 0, .0)','box-shadow':'0 0 50px rgba(0,0,0,.0)'});
      document.getElementById("top").style.display = "none";
   }
})

//go to top of the page
function topFunction() {
  document.documentElement.scrollTop = 0;
}