from django.urls import path
from . import views


urlpatterns = [
    path('', views.home, name='home-home'),
    path('about/', views.about, name='home-about'),
    path('books/', views.books, name='home-books'),
    path('authors/', views.authors, name='home-authors'),
    path('exactbook/<int:id>', views.exactbook, name='home-exactbook'),
    path('exactauthor/<int:id>', views.exactauthor, name='home-exactauthor'),
]
