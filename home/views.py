from django.shortcuts import render
# from django.http import HttpResponse
from .models import Author, Book


def home(request):
    param = {'title': 'Home'}
    return render(request, 'home/home.html', param)


def about(request):
    param = {'title': 'About'}
    return render(request, 'home/about.html', param)


def books(request):
    param = {
        'title': 'Books',
        'items': Book.objects.all()
    }
    return render(request, 'home/books.html', param)


def authors(request):
    param = {
        'title': 'Authors',
        'items': Author.objects.all()
    }
    return render(request, 'home/authors.html', param)


def exactbook(request, id):
    eb = Book.objects.get(id=id)
    param = {
        'title': eb.name,
        'item': eb
    }
    return render(request, 'home/exactbook.html', param)


def exactauthor(request, id):
    ea = Author.objects.get(id=id)
    param = {
        'title': ea.name,
        'item': ea
    }
    return render(request, 'home/exactauthor.html', param)
