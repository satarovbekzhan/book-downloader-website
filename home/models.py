from django.db import models
from django.contrib.auth.models import User


class Author(models.Model):
	name = models.CharField(max_length=100)
	land = models.CharField(max_length=200)
	born = models.DateField()
	dead = models.DateField()
	biography = models.TextField()
	img = models.CharField(max_length=200)

	def __str__(self):
		return self.name


class Genre(models.Model):
	name = models.CharField(max_length=80)

	def __str__(self):
		return self.name


class Book(models.Model):
	name = models.CharField(max_length=200)
	written = models.DateField()
	author = models.ForeignKey(Author, on_delete=models.CASCADE)
	genre = models.ForeignKey(Genre, on_delete=models.CASCADE)
	demo = models.TextField()
	img = models.CharField(max_length=200)
	full = models.CharField(max_length=255)

	def __str__(self):
		return self.name


class Like(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	book = models.ForeignKey(Book, on_delete=models.CASCADE)
